import { Router } from "express";
import {
  createInventario,
  deleteInventario,
  getInventario,
  getInventarioById,
  updateInventario,
} from "../controllers/inventario.controllers";

const router = Router();

router.get("/", getInventario);

router.get("/:id", getInventarioById);

router.post("/", createInventario);

router.put("/", updateInventario);

router.delete("/", deleteInventario);

export default router;
