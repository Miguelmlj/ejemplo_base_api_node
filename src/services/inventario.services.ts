export class InventarioService {
  get() {
    return "GET Inventario";
  }

  getById(id: string) {
    return `GET Inventario ${id}`
  }

  create() {
    return "POST Inventario";  
  }

  update() {
    return "UPDATE Inventario";
  }

  delete() {
    return "DELETE Inventario";
  }
}

