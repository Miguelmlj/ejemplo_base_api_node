import express, { Request, Response } from "express";

import inventarioRouter from "./routes/inventario.routes";

const app = express();

app.use(express.json()); // application/json

// Con esta linea le indicamos al server que todas las rutas con prefijo
// inventario iran al router de inventario
app.use("/inventario", inventarioRouter);

app.get("/", (req: Request, res: Response) => {
  res.send("GET /");
})

app.listen(3000, function () {
  console.log("Servidor corriendo");
});
