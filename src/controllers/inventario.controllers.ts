import { Request, Response } from "express";
import { InventarioService } from "../services/inventario.services";

const inventarioService = new InventarioService();

export function getInventario(req: Request, res: Response) {
  res.send(inventarioService.get());
}

export function getInventarioById(req: Request, res: Response) {
  const { id } = req.params;
  res.send(inventarioService.getById(id));
}

export function createInventario(req: Request, res: Response) {
  res.send(inventarioService.create());
}

export function updateInventario(req: Request, res: Response) {
  res.send(inventarioService.update());
}

export function deleteInventario(req: Request, res: Response) {
  res.send(inventarioService.delete());
}
